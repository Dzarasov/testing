package com.seleniumtests.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

WebDriver driver;
	
	public HomePage(WebDriver driver){
		this.driver=driver;
	}
	

	public void clickOnHomeButton(){
	WebElement homeButton = driver.findElement(By.cssSelector("a[href='#home']"));
	homeButton.click();
	}

	
	public IPInformationPage clickOnIpInformationButtonToRedirectToIPpage(){
		WebElement ip = driver.findElement(By.cssSelector("a[href='#ip']"));
		ip.click();
		WebElement waitTillIpLooKupButtonPresented = (new WebDriverWait(driver, 300))
				   .until(ExpectedConditions.visibilityOfElementLocated(By.id("iplookup-tab")));	
		if(waitTillIpLooKupButtonPresented.isDisplayed()){
			return new IPInformationPage(driver);

		}
				System.out.println("waitTillIpLooKupButtonPresented is not visible need wait more...");
				return null;
				
		}

	public void signOut(){
		WebElement homeButton = driver.findElement(By.cssSelector("a[href='#home']"));
		homeButton.click();
		//<a id="login-button"
	}
	
}
