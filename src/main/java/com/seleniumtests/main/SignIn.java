package com.seleniumtests.main;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignIn {
	
	protected WebDriver driver;
	private String textForSignIn;
	
	public SignIn(WebDriver driver){
		this.driver=driver;
	}
	
	/*public String getTextForSignIn() {
		return textForSignIn;
	}


	public void setTextForSignIn(String textForSignIn) {
		this.textForSignIn = textForSignIn;
	}*/

	
	
	public SignInPageObject signIn() throws InterruptedException
	{
		
		WebElement alreadyAMemberButton = driver.findElement(By.id("existing-member-reglink"));
		
		alreadyAMemberButton.click();
		
		WebElement element = (new WebDriverWait(driver, 300))
				   .until(ExpectedConditions.visibilityOfElementLocated(By.id("custom-content-left")));
		
		if(element.isDisplayed()){
			return new SignInPageObject(driver);
		}
		System.out.println("neustar element was not found");
		return null;
		
		
		
	
		
		
		//WebElement login = driver.findElement(By.id("login-email"));
		//login.sendKeys("dzaras777@yahoo.com");
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//WebElement verifyEmail = driver.findElement(By.cssSelector("span[class='validate-icon asterisk']"));
		//verifyEmail.click();
		//WebElement signInButton = driver.findElement(By.className("signin-link"));
		//signInButton.click();
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//WebElement textVerify = driver.findElement(By.cssSelector("div[id='custom-content-left']"));
		//String str = textVerify.getText();
		//setTextForSignIn(str);
		
	}
}
