package com.seleniumtests.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ModuleValidationAfterSignIn {
	
	
	WebDriver driver;
	
	public ModuleValidationAfterSignIn(WebDriver driver){
		this.driver = driver;
	}
	
	public boolean homeModulesOnTheHeader(){
		WebElement homeModule = driver.findElement(By.cssSelector("a[href='#home']"));
		if(homeModule.isDisplayed()){
			return true;
		}
		return false;
	}
	
	
	public boolean ipInformationModulesOnTheHeader(){
		WebElement ipInformationModule = driver.findElement(By.cssSelector("a[href='#ip']"));
		if(ipInformationModule.isDisplayed()){
			return true;
		}
		return false;
	}
	
	
	public boolean myAccountModulesOnTheHeader(){
		WebElement myAccountModule = driver.findElement(By.cssSelector("a[href='#myaccount']"));
		if(myAccountModule.isDisplayed()){
			return true;
		}
		return false;
	}
	
	public boolean licenseModulesOnTheHeader(){
		WebElement licenseModule = driver.findElement(By.cssSelector("a[href='#licenses']"));
		if(licenseModule.isDisplayed()){
			return true;
		}
		return false;
	}
	
	
	public boolean documentationModulesOnTheHeader(){
		WebElement documentationModule = driver.findElement(By.cssSelector("a[href='#documentation']"));
		if(documentationModule.isDisplayed()){
			return true;
		}
		return false;
	}
	
	
	
	public boolean extentionModulesOnTheHeader(){
		WebElement extentionModule = driver.findElement(By.cssSelector("a[href='#extensions']"));
		if(extentionModule.isDisplayed()){
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

