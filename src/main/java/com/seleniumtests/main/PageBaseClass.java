package com.seleniumtests.main;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SubjectTerm;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageBaseClass {

	WebDriver driver;
	
	public static void registeredEmailAddressSendKeys(WebDriver driver){
		WebElement login = driver.findElement(By.id("login-email"));
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		//this email already registered in production IPI
		login.sendKeys("dzaras777@yahoo.com");
	}
	
	
	public static String getValueFromTestDataFile(String value) {
		Properties prop = new Properties();
		String selectedItem = null;

		try {
			FileInputStream fileInputStream = new FileInputStream(
					"test_data.properties");
			prop.load(fileInputStream);
			selectedItem = prop.getProperty(value);
			fileInputStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return selectedItem;
	}
	
	
	public static String getVerificationLink(String emailLogin,
			String emailPassword, String userName, String subject, String start, String end) throws Exception {
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore("imaps");
		store.connect("imap.gmail.com", emailLogin, emailPassword);

		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_WRITE);

		Message[] messages = null;
		boolean isMailFound = false;
		Message verificatoinMail = null;

		for (int i = 0; i < 5; i++) {
			messages = folder.search(new SubjectTerm(subject/*"Neustar - Password Reset Request"*/),folder.getMessages());   //("Neustar IP Intelligence User Activation"), 
					

			// System.out.println(messages.length);
			if (messages.length == 0) {
				Thread.sleep(10000);
			} else {
				break;
			}
		}

		for (Message mail : messages) {
			if (!mail.isSet(Flags.Flag.SEEN)) {
				verificatoinMail = mail;
				String line = "";
				StringBuffer buffer = new StringBuffer();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(verificatoinMail.getInputStream()));
				while ((line = reader.readLine()) != null) {
					// Reporter.log(line);
					buffer.append(line);
				}//System.out.println(buffer);
				if (buffer.indexOf(userName) != -1) {
					isMailFound = true;
					break;
				}
			}
		}
		StringBuffer buffer = null;
		
		//if (!isMailFound) {
		//	throw new Exception("No new emails found");
		//} else {
			String line;
			 buffer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					verificatoinMail.getInputStream()));
			while ((line = reader.readLine()) != null) {
			buffer.append(line);

		}
			
		
		  String searchedLink = start;//"href=\"";          //"Activate registration<"; 
		  int linkStart = buffer.indexOf(searchedLink)+ searchedLink.length(); 
		  
		  int linkEnd = buffer.indexOf(end/*"\" style="*/, linkStart);//(">Sincerely", linkStart); 
		  
		  String link = buffer.substring(linkStart, linkEnd);

		  //verificatoinMail.setFlag(Flags.Flag.DELETED, true);
		  folder.close(true); 
		System.out.println(link);
		  return link;
		//	System.out.println(buffer.toString());
	//return buffer.toString();		
	}
	
	
	//http://platformqa01.qa.quova.com/apps/pr/rp?token=4af4f3bad12cf322694101082648521d01f29eb69a4deb8f18053327a913e1e0&CL=gp.od.dev.nsr	
	
    //http://platformqa01.qa.quova.com/apps/pr/rp?token=c2b14e1c0b231eb1d0865f2573bf1616ff4e4cc796797d89d09aa0319f415df0&CL=gp.od.dev.nsr" style="color:#ff530c
	
	
	
	
	
	
	
	
	
	
	
	
	
	//working code
//	public static String getVerificationLink2(String emailLogin,
//			String emailPassword, String userName) throws Exception {
//		Properties props = System.getProperties();
//		props.setProperty("mail.store.protocol", "imaps");
//
//		Session session = Session.getDefaultInstance(props, null);
//		Store store = session.getStore("imaps");
//		store.connect("imap.gmail.com", emailLogin, emailPassword);
//
//		Folder folder = store.getFolder("INBOX");
//		folder.open(Folder.READ_WRITE);
//
//		Message[] messages = null;
//		boolean isMailFound = false;
//		Message verificatoinMail = null;
//
//		for (int i = 0; i < 5; i++) {
//			messages = folder.search(new SubjectTerm("Neustar IP Intelligence User Activation"), folder.getMessages());
//
//			// System.out.println(messages.length);
//			if (messages.length == 0) {
//				Thread.sleep(10000);
//			} else {
//				break;
//			}
//		}
//
//		for (Message mail : messages) {
//			if (!mail.isSet(Flags.Flag.SEEN)) {
//				verificatoinMail = mail;
//				String line = "";
//				StringBuffer buffer = new StringBuffer();
//				BufferedReader reader = new BufferedReader(
//						new InputStreamReader(verificatoinMail.getInputStream()));
//				while ((line = reader.readLine()) != null) {
//					// Reporter.log(line);
//					buffer.append(line);
//				}//System.out.println(buffer);
//				if (buffer.indexOf(userName) != -1) {
//					isMailFound = true;
//					break;
//				}
//			}
//		}
//		StringBuffer buffer = null;
//		
//		//if (!isMailFound) {
//		//	throw new Exception("No new emails found");
//		//} else {
//			String line;
//			 buffer = new StringBuffer();
//			BufferedReader reader = new BufferedReader(new InputStreamReader(
//					verificatoinMail.getInputStream()));
//			while ((line = reader.readLine()) != null) {
//			buffer.append(line);
//
//		}
//			
//		
//		  String searchedLink = "Activate registration<"; 
//		  int linkStart = buffer.indexOf(searchedLink)+ searchedLink.length(); 
//		  
//		  int linkEnd = buffer.indexOf(">Sincerely", linkStart); 
//		  
//		  String link = buffer.substring(linkStart, linkEnd);
//
//		  //verificatoinMail.setFlag(Flags.Flag.DELETED, true);
//		  folder.close(true); 
//		System.out.println(link);
//		  return link;
//		//	System.out.println(buffer.toString());
//	//return buffer.toString();		
//	}
//	
	
	
	
	
	
}
